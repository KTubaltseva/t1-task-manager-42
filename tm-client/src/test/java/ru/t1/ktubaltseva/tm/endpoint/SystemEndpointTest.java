package ru.t1.ktubaltseva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.endpoint.ISystemEndpoint;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.ktubaltseva.tm.dto.request.system.ApplicationVersionRequest;
import ru.t1.ktubaltseva.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.ktubaltseva.tm.dto.response.system.ApplicationVersionResponse;
import ru.t1.ktubaltseva.tm.marker.IntegrationCategory;
import ru.t1.ktubaltseva.tm.service.LoggerService;
import ru.t1.ktubaltseva.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class SystemEndpointTest {

    @NotNull
    private final static ILoggerService loggerService = new LoggerService();

    @NotNull
    private final static IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private final static ISystemEndpoint endpoint = ISystemEndpoint.newInstance(propertyService);


    @BeforeClass
    public static void before() {
    }

    @AfterClass
    public static void after() {
    }

    @Test
    public void getAbout() {
        @NotNull final ApplicationAboutResponse response = endpoint.getAbout(new ApplicationAboutRequest());
        Assert.assertNotNull(response);
    }

    @Test
    public void getVersion() {
        @NotNull final ApplicationVersionResponse response = endpoint.getVersion(new ApplicationVersionRequest());
        Assert.assertNotNull(response);
    }

}
