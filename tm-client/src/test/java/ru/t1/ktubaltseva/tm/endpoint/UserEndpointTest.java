package ru.t1.ktubaltseva.tm.endpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ktubaltseva.tm.api.endpoint.IUserEndpoint;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.dto.request.user.*;
import ru.t1.ktubaltseva.tm.dto.response.user.*;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.marker.IntegrationCategory;
import ru.t1.ktubaltseva.tm.service.LoggerService;
import ru.t1.ktubaltseva.tm.service.PropertyService;

import java.security.NoSuchAlgorithmException;

import static ru.t1.ktubaltseva.tm.constant.UserTestData.*;

@Category(IntegrationCategory.class)
public final class UserEndpointTest {

    @NotNull
    private final static ILoggerService loggerService = new LoggerService();

    @NotNull
    private final static IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private final static IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final static IUserEndpoint endpoint = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @BeforeClass
    public static void before() throws AbstractException, NoSuchAlgorithmException, JsonProcessingException {
        @NotNull final UserLoginRequest loginAdminRequest = new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD);
        @NotNull final UserLoginResponse loginAdminResponse = authEndpoint.login(loginAdminRequest);
        adminToken = loginAdminResponse.getToken();

        @NotNull final UserRegistryRequest request = new UserRegistryRequest(USER_LOGIN, USER_PASSWORD, USER_EMAIL);
        endpoint.registry(request);

        @NotNull final UserLoginRequest loginUserRequest = new UserLoginRequest(USER_LOGIN, USER_PASSWORD);
        @NotNull final UserLoginResponse loginUserResponse = authEndpoint.login(loginUserRequest);
        userToken = loginUserResponse.getToken();
    }

    @AfterClass
    public static void after() throws AbstractException {
        @NotNull final UserLogoutRequest logoutUserRequest = new UserLogoutRequest(userToken);
        authEndpoint.logout(logoutUserRequest);
        userToken = null;

        @NotNull final UserRemoveRequest removeUserRequest = new UserRemoveRequest(adminToken, USER_LOGIN);
        endpoint.remove(removeUserRequest);

        @NotNull final UserLogoutRequest logoutAdminRequest = new UserLogoutRequest(adminToken);
        authEndpoint.logout(logoutAdminRequest);
        adminToken = null;
    }

    @Test
    public void changePassword() throws AbstractException, NoSuchAlgorithmException {
        @NotNull final String newPassword = "newPassword";
        @NotNull final UserChangePasswordResponse response = endpoint.changePassword(new UserChangePasswordRequest(userToken, newPassword));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getUser());

        endpoint.changePassword(new UserChangePasswordRequest(userToken, USER_PASSWORD));
    }

    @Test
    public void lock() throws AbstractException {
        @NotNull final UserLockResponse response = endpoint.lock(new UserLockRequest(adminToken, USER_LOGIN));
        Assert.assertNotNull(response);

        endpoint.unlock(new UserUnlockRequest(adminToken, USER_LOGIN));
    }

    @Test
    public void registry() throws AbstractException, NoSuchAlgorithmException {
        @NotNull final UserRegistryResponse response = endpoint.registry(new UserRegistryRequest(USER_REGISTRY_LOGIN, USER_REGISTRY_PASSWORD, USER_EMAIL));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getUser());

        endpoint.remove(new UserRemoveRequest(adminToken, USER_REGISTRY_LOGIN));
    }

    @Test
    public void remove() throws AbstractException, NoSuchAlgorithmException {
        endpoint.registry(new UserRegistryRequest(USER_REGISTRY_LOGIN, USER_REGISTRY_PASSWORD, USER_REGISTRY_PASSWORD));
        @NotNull final UserRemoveResponse response = endpoint.remove(new UserRemoveRequest(adminToken, USER_REGISTRY_LOGIN));
        Assert.assertNotNull(response);
    }

    @Test
    public void unlock() throws AbstractException {
        endpoint.lock(new UserLockRequest(adminToken, USER_LOGIN));
        @NotNull final UserUnlockResponse response = endpoint.unlock(new UserUnlockRequest(adminToken, USER_LOGIN));
        Assert.assertNotNull(response);
    }

    @Test
    public void updateProfile() throws AbstractException {
        endpoint.lock(new UserLockRequest(adminToken, USER_LOGIN));
        @NotNull final UserUpdateProfileResponse response = endpoint.updateProfile(new UserUpdateProfileRequest(userToken, USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getUser());
    }

}
