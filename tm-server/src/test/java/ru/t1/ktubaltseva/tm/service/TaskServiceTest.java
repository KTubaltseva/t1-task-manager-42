package ru.t1.ktubaltseva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.*;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;

import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.TaskTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private static final ILoggerService loggerService = new LoggerService();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);


    @NotNull
    private final ITaskService service = new TaskService(connectionService);

    @NotNull
    private static final IUserService userService = new UserService(propertyService, connectionService);

    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        userService.add(USER_1);
        userService.add(USER_2);
        projectService.add(USER_1_PROJECT_1);
        projectService.add(USER_1_PROJECT_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        userService.removeOne(USER_1);
        userService.removeOne(USER_2);
    }

    @After
    @SneakyThrows
    public void after() {
        service.clear(USER_1.getId());
        service.clear(USER_2.getId());
        projectService.clear(USER_1.getId());
        projectService.clear(USER_2.getId());
    }

    @Test
    public void add() throws AbstractException {
        Assert.assertThrows(TaskNotFoundException.class, () -> service.add(NULL_TASK));

        @Nullable final TaskDTO taskToAdd = USER_1_TASK_1;
        @Nullable final String taskToAddId = taskToAdd.getId();

        @Nullable final TaskDTO taskAdded = service.add(taskToAdd);
        Assert.assertNotNull(taskAdded);
        Assert.assertEquals(taskToAdd.getId(), taskAdded.getId());

        @Nullable final TaskDTO taskFindOneById = service.findOneById(taskToAddId);
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskToAdd.getId(), taskFindOneById.getId());
    }

    @Test
    public void addByUserId() throws AbstractException {
        Assert.assertThrows(TaskNotFoundException.class, () -> service.add(USER_1.getId(), NULL_TASK));
        Assert.assertThrows(AuthRequiredException.class, () -> service.add(NULL_USER_ID, USER_1_TASK_1));

        @Nullable final String userToAddId = USER_1.getId();
        @Nullable final String userNoAddId = USER_2.getId();
        @Nullable final TaskDTO taskToAddByUser = USER_1_TASK_1;
        @Nullable final String taskToAddByUserId = taskToAddByUser.getId();

        @Nullable final TaskDTO taskAddedByUser = service.add(userToAddId, taskToAddByUser);
        Assert.assertNotNull(taskAddedByUser);
        Assert.assertTrue(service.existsById(taskToAddByUserId));

        @Nullable final TaskDTO taskFindOneById = service.findOneById(taskToAddByUserId);
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskAddedByUser.getId(), taskFindOneById.getId());

        @Nullable final TaskDTO taskFindOneByIdByUserIdToAdd = service.findOneById(userToAddId, taskToAddByUserId);
        Assert.assertNotNull(taskFindOneByIdByUserIdToAdd);
        Assert.assertEquals(taskAddedByUser.getId(), taskFindOneByIdByUserIdToAdd.getId());

        Assert.assertThrows(TaskNotFoundException.class, () -> service.findOneById(userNoAddId, taskToAddByUserId));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.findOneById(userToAddId, NON_EXISTENT_TASK_ID));
    }

    @Test
    public void addMany() throws AbstractException {
        @Nullable final Collection<TaskDTO> taskList = service.add(TASK_LIST);
        Assert.assertNotNull(taskList);
        for (@NotNull final TaskDTO task : TASK_LIST) {
            @Nullable final TaskDTO taskFindOneById = service.findOneById(task.getId());
            Assert.assertEquals(task.getId(), taskFindOneById.getId());
        }
    }

    @Test
    public void findOneById() throws AbstractException {
        @NotNull final TaskDTO taskExists = USER_1_TASK_1;
        service.add(taskExists);

        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(NULL_TASK_ID));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.findOneById(NON_EXISTENT_TASK_ID));

        @Nullable final TaskDTO taskFindOneById = service.findOneById(taskExists.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskExists.getId(), taskFindOneById.getId());
    }

    @Test
    public void findOneByIdByUserId() throws AbstractException {
        @NotNull final TaskDTO taskExists = USER_1_TASK_1;
        @NotNull final UserDTO userExists = USER_1;
        service.add(userExists.getId(), taskExists);

        Assert.assertThrows(AuthRequiredException.class, () -> service.findOneById(NULL_USER_ID, taskExists.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userExists.getId(), NULL_TASK_ID));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.findOneById(userExists.getId(), NON_EXISTENT_TASK_ID));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.findOneById(NON_EXISTENT_USER_ID, taskExists.getId()));

        @Nullable final TaskDTO taskFindOneById = service.findOneById(userExists.getId(), taskExists.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskExists.getId(), taskFindOneById.getId());
    }

    @Test
    public void findAll() throws AbstractException {
        @NotNull final TaskDTO taskExists = USER_1_TASK_1;
        service.add(taskExists);
        @NotNull final Collection<TaskDTO> tasksFindAllNoEmpty = service.findAll();
        Assert.assertNotNull(tasksFindAllNoEmpty);
    }

    @Test
    public void findAllByUserId() throws AbstractException {
        Assert.assertThrows(AuthRequiredException.class, () -> service.findAll(NULL_USER_ID));

        @NotNull final TaskDTO taskExists = USER_1_TASK_1;
        @NotNull final UserDTO userExists = USER_1;

        service.add(userExists.getId(), taskExists);
        @NotNull final Collection<TaskDTO> tasksFindAllByUserRepNoEmpty = service.findAll(userExists.getId());
        Assert.assertNotNull(tasksFindAllByUserRepNoEmpty);

        @NotNull final Collection<TaskDTO> tasksFindAllByNonExistentUser = service.findAll(NON_EXISTENT_USER_ID);
        Assert.assertNotNull(tasksFindAllByNonExistentUser);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentUser);
    }

    @Test
    public void clearByUserId() throws AbstractException {
        Assert.assertThrows(AuthRequiredException.class, () -> service.clear(NULL_USER_ID));

        @NotNull final UserDTO userToClear = USER_1;
        @NotNull final UserDTO userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final Collection<TaskDTO> taskByUserToClearList = service.add(USER_1_TASK_LIST);
        @NotNull final Collection<TaskDTO> taskByUserNoClearList = service.add(USER_2_TASK_LIST);
        service.clear(userToClearId);

        for (@NotNull final TaskDTO taskByUserToClear : taskByUserToClearList) {
            Assert.assertThrows(TaskNotFoundException.class, () -> service.findOneById(taskByUserToClear.getId()));
        }
        Assert.assertEquals(0, service.findAll(userToClearId).size());

        for (@NotNull final TaskDTO taskByUserNoClear : taskByUserNoClearList) {
            @Nullable final TaskDTO taskFindOneById = service.findOneById(taskByUserNoClear.getId());
            Assert.assertEquals(taskByUserNoClear.getId(), taskFindOneById.getId());
        }
        Assert.assertNotEquals(0, service.findAll(userNoClearId).size());
    }

    @Test
    public void removeOne() throws AbstractException {
        @Nullable final TaskDTO taskToRemove = USER_1_TASK_1;
        service.add((taskToRemove));

        Assert.assertThrows(TaskNotFoundException.class, () -> service.removeOne(NULL_TASK));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.removeOne(NON_EXISTENT_TASK));

        service.removeOne(taskToRemove);

        Assert.assertThrows(TaskNotFoundException.class, () -> service.findOneById(taskToRemove.getId()));
    }

    @Test
    public void removeOneByUserId() throws AbstractException {
        @NotNull final UserDTO userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final TaskDTO taskByUserToRemove = service.add((USER_1_TASK_1));
        @Nullable final TaskDTO taskByUserNoRemove = service.add((USER_2_TASK_1));

        Assert.assertThrows(TaskNotFoundException.class, () -> service.removeOne(NON_EXISTENT_USER_ID, taskByUserToRemove));
        Assert.assertThrows(AuthRequiredException.class, () -> service.removeOne(NULL_USER_ID, taskByUserToRemove));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.removeOne(userToRemoveId, NULL_TASK));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.removeOne(userToRemoveId, NON_EXISTENT_TASK));

        service.removeOne(userToRemoveId, taskByUserToRemove);

        Assert.assertThrows(TaskNotFoundException.class, () -> service.findOneById(taskByUserToRemove.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.removeOne(userToRemoveId, taskByUserNoRemove));

        @Nullable final TaskDTO taskNoRemovedFindOneById = service.findOneById(taskByUserNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById.getId(), taskByUserNoRemove.getId());
    }

    @Test
    public void removeById() throws AbstractException {
        @Nullable final TaskDTO taskToRemove = USER_1_TASK_1;
        service.add((taskToRemove));

        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(NULL_TASK_ID));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.removeById(NON_EXISTENT_TASK_ID));

        service.removeById(taskToRemove.getId());

        Assert.assertThrows(TaskNotFoundException.class, () -> service.findOneById(taskToRemove.getId()));
    }

    @Test
    public void removeByIdByUserId() throws AbstractException {
        @NotNull final UserDTO userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final TaskDTO taskByUserToRemove = service.add((USER_1_TASK_1));
        @Nullable final TaskDTO taskByUserNoRemove = service.add((USER_2_TASK_1));

        Assert.assertThrows(TaskNotFoundException.class, () -> service.removeById(NON_EXISTENT_USER_ID, taskByUserToRemove.getId()));
        Assert.assertThrows(AuthRequiredException.class, () -> service.removeById(NULL_USER_ID, taskByUserToRemove.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userToRemoveId, NULL_TASK_ID));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.removeById(userToRemoveId, NON_EXISTENT_TASK_ID));

        service.removeById(userToRemoveId, taskByUserToRemove.getId());

        Assert.assertThrows(TaskNotFoundException.class, () -> service.findOneById(taskByUserToRemove.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.removeById(userToRemoveId, taskByUserNoRemove.getId()));

        @Nullable final TaskDTO taskNoRemovedFindOneById = service.findOneById(taskByUserNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById.getId(), taskByUserNoRemove.getId());
    }

    @Test
    public void isExists() throws AbstractException {
        @NotNull final TaskDTO taskExists = USER_1_TASK_1;
        service.add(taskExists);

        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(NULL_TASK_ID));

        Assert.assertFalse(service.existsById(NON_EXISTENT_TASK_ID));
        Assert.assertTrue(service.existsById(taskExists.getId()));
    }

    @Test
    public void createName() throws AbstractException {
        @NotNull final UserDTO existentUser = USER_1;
        Assert.assertThrows(AuthRequiredException.class, () -> service.create(NULL_USER_ID, TASK_NAME));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(existentUser.getId(), NULL_NAME));

        @NotNull final TaskDTO createdTask = service.create(existentUser.getId(), TASK_NAME);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(TASK_NAME, createdTask.getName());
        Assert.assertTrue(service.existsById(createdTask.getId()));

        @Nullable final TaskDTO taskFindOneById = service.findOneById(createdTask.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(createdTask.getId(), taskFindOneById.getId());
    }

    @Test
    public void createNameDesc() throws AbstractException {
        @NotNull final UserDTO existentUser = USER_1;
        Assert.assertThrows(AuthRequiredException.class, () -> service.create(NULL_USER_ID, TASK_NAME, TASK_DESC));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(existentUser.getId(), NULL_NAME, TASK_DESC));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(existentUser.getId(), TASK_NAME, NULL_DESC));

        @NotNull final TaskDTO createdTask = service.create(existentUser.getId(), TASK_NAME, TASK_DESC);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(TASK_NAME, createdTask.getName());
        Assert.assertEquals(TASK_DESC, createdTask.getDescription());
        Assert.assertTrue(service.existsById(createdTask.getId()));

        @Nullable final TaskDTO taskFindOneById = service.findOneById(createdTask.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(createdTask.getId(), taskFindOneById.getId());
    }

    @Test
    public void changeTaskStatusById() throws AbstractException {
        @NotNull final UserDTO userToUpdate = USER_1;
        @NotNull final UserDTO userNoUpdate = USER_2;
        @Nullable final TaskDTO taskToUpdate = service.add((USER_1_TASK_1));
        @Nullable final TaskDTO taskNoUpdate = service.add((USER_1_TASK_2));

        Assert.assertThrows(AuthRequiredException.class, () -> service.changeTaskStatusById(NULL_USER_ID, taskToUpdate.getId(), TASK_STATUS));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.changeTaskStatusById(NON_EXISTENT_USER_ID, taskToUpdate.getId(), TASK_STATUS));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.changeTaskStatusById(userNoUpdate.getId(), taskToUpdate.getId(), TASK_STATUS));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.changeTaskStatusById(userToUpdate.getId(), NULL_TASK_ID, TASK_STATUS));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.changeTaskStatusById(userToUpdate.getId(), NON_EXISTENT_TASK_ID, TASK_STATUS));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeTaskStatusById(userToUpdate.getId(), taskToUpdate.getId(), NULL_STATUS));

        @Nullable final TaskDTO taskUpdated = service.changeTaskStatusById(userToUpdate.getId(), taskToUpdate.getId(), TASK_STATUS);
        Assert.assertNotNull(taskUpdated);
        Assert.assertEquals(taskUpdated.getId(), taskToUpdate.getId());
        Assert.assertEquals(TASK_STATUS, taskUpdated.getStatus());

        @Nullable final TaskDTO taskFindOneByIdToUpdate = service.findOneById(taskToUpdate.getId());
        Assert.assertNotNull(taskFindOneByIdToUpdate);
        Assert.assertEquals(taskFindOneByIdToUpdate.getId(), taskToUpdate.getId());
        Assert.assertEquals(TASK_STATUS, taskFindOneByIdToUpdate.getStatus());

        @Nullable final TaskDTO taskFindOneByIdNoUpdate = service.findOneById(taskNoUpdate.getId());
        Assert.assertNotNull(taskFindOneByIdNoUpdate);
        Assert.assertEquals(taskFindOneByIdNoUpdate.getId(), taskNoUpdate.getId());
        Assert.assertNotEquals(TASK_STATUS, taskFindOneByIdNoUpdate.getStatus());
    }

    @Test
    public void updateTaskById() throws AbstractException {
        @NotNull final UserDTO userToUpdate = USER_1;
        @NotNull final UserDTO userNoUpdate = USER_2;
        @Nullable final TaskDTO taskToUpdate = service.add((USER_1_TASK_1));
        @Nullable final TaskDTO taskNoUpdate = service.add((USER_1_TASK_2));

        Assert.assertThrows(AuthRequiredException.class, () -> service.updateById(NULL_USER_ID, taskToUpdate.getId(), TASK_NAME, TASK_DESC));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.updateById(NON_EXISTENT_USER_ID, taskToUpdate.getId(), TASK_NAME, TASK_DESC));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.updateById(userNoUpdate.getId(), taskToUpdate.getId(), TASK_NAME, TASK_DESC));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.updateById(userToUpdate.getId(), NULL_TASK_ID, TASK_NAME, TASK_DESC));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.updateById(userToUpdate.getId(), NON_EXISTENT_TASK_ID, TASK_NAME, TASK_DESC));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(userToUpdate.getId(), taskToUpdate.getId(), NULL_NAME, TASK_DESC));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateById(userToUpdate.getId(), taskToUpdate.getId(), TASK_NAME, NULL_DESC));

        @Nullable final TaskDTO taskUpdated = service.updateById(userToUpdate.getId(), taskToUpdate.getId(), TASK_NAME, TASK_DESC);
        Assert.assertNotNull(taskUpdated);
        Assert.assertEquals(taskUpdated.getId(), taskToUpdate.getId());
        Assert.assertEquals(TASK_NAME, taskUpdated.getName());
        Assert.assertEquals(TASK_DESC, taskUpdated.getDescription());

        @Nullable final TaskDTO taskFindOneByIdToUpdate = service.findOneById(taskToUpdate.getId());
        Assert.assertNotNull(taskFindOneByIdToUpdate);
        Assert.assertEquals(taskFindOneByIdToUpdate.getId(), taskToUpdate.getId());
        Assert.assertEquals(TASK_NAME, taskFindOneByIdToUpdate.getName());
        Assert.assertEquals(TASK_DESC, taskFindOneByIdToUpdate.getDescription());

        @Nullable final TaskDTO taskFindOneByIdNoUpdate = service.findOneById(taskNoUpdate.getId());
        Assert.assertNotNull(taskFindOneByIdNoUpdate);
        Assert.assertEquals(taskFindOneByIdNoUpdate.getId(), taskNoUpdate.getId());
        Assert.assertNotEquals(TASK_NAME, taskFindOneByIdNoUpdate.getName());
        Assert.assertNotEquals(TASK_DESC, taskFindOneByIdNoUpdate.getDescription());
    }

}
