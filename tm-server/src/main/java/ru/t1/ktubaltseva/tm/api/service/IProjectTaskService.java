package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.TaskDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public interface IProjectTaskService {

    @NotNull
    TaskDTO bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws AbstractException;

    @NotNull
    TaskDTO unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws AbstractException;

    void removeProjectById(
            @Nullable String userId,
            @Nullable String projectId
    ) throws AbstractException;

    void clearProjects(
            @Nullable String userId
    ) throws AbstractException;

}
