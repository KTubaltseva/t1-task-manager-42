package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import java.security.NoSuchAlgorithmException;

public interface IUserService extends IService<UserDTO> {

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password
    ) throws NoSuchAlgorithmException, AbstractException;

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws AbstractException, NoSuchAlgorithmException;

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    ) throws AbstractException, NoSuchAlgorithmException;

    @NotNull
    UserDTO findByLogin(@Nullable String login) throws AbstractException;

    @NotNull
    UserDTO findByEmail(@Nullable String email) throws AbstractException;

    @NotNull
    Boolean isLoginExists(@Nullable String login) throws AbstractException;

    @NotNull
    Boolean isEmailExists(@Nullable String email) throws AbstractException;

    @NotNull
    UserDTO lockUserByLogin(@Nullable String login) throws AbstractException;

    void removeByLogin(@Nullable String login) throws AbstractException;

    void removeByEmail(@Nullable String email) throws AbstractException;

    @NotNull
    UserDTO setPassword(
            @Nullable String id,
            @Nullable String password
    ) throws NoSuchAlgorithmException, AbstractException;

    @NotNull
    UserDTO unlockUserByLogin(@Nullable String login) throws AbstractException;

    @NotNull UserDTO update(
            @Nullable UserDTO user
    ) throws AbstractException;

    @NotNull
    UserDTO updateById(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String lastName
    ) throws AbstractException;
}
