package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.dto.model.SessionDTO;

public interface ISessionService extends IUserOwnedService<SessionDTO> {
}
