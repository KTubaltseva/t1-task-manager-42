package ru.t1.ktubaltseva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.project.*;
import ru.t1.ktubaltseva.tm.dto.response.project.*;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import static ru.t1.ktubaltseva.tm.api.endpoint.IEndpoint.*;

@WebService
public interface IProjectEndpoint {

    @NotNull
    String NAME = "ProjectEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IProjectEndpoint.class);
    }

    @NotNull
    @WebMethod
    ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectClearResponse clearProjects(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectCompleteByIdResponse completeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectDisplayByIdResponse getProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectDisplayByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectDisplayListResponse getAllProjects(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectDisplayListRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectStartByIdResponse startProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) throws AbstractException;

}
