package ru.t1.ktubaltseva.tm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Setter
@Getter
@MappedSuperclass
@NoArgsConstructor
public class AbstractModel implements Serializable {

    @Id
    @NotNull
    @Column(name = "id")
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(name = "created")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    private Date created = new Date();

}
