package ru.t1.ktubaltseva.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.exception.field.RoleEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.RoleIncorrectException;

@Getter
public enum Role {

    USUAL("Usual user"),
    ADMIN("Admin user");

    private @NotNull
    final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    @Override
    public String toString() {
        return name();
    }


    @NotNull
    public static Role toRole(@Nullable final String value) throws RoleEmptyException, RoleIncorrectException {
        if (value == null || value.isEmpty()) throw new RoleEmptyException();
        for (@NotNull final Role role : values()) {
            if (role.name().equals(value)) return role;
        }
        throw new RoleIncorrectException();
    }
}
