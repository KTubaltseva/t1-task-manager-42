package ru.t1.ktubaltseva.tm.dto.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.model.IWBS;
import ru.t1.ktubaltseva.tm.enumerated.Status;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "project")
@Table(name = "tm_project")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class ProjectDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private static final long serialVersionUID = 0;

    @NotNull
    @Column(name = "name", columnDefinition = "varchar(30)", nullable = false)
    private String name = "";

    @Nullable
    @Column(name = "description", columnDefinition = "varchar(255)")
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", columnDefinition = "varchar(30)", nullable = false)
    private Status status = Status.NOT_STARTED;

    public ProjectDTO(@NotNull final String name) {
        this.name = name;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public ProjectDTO(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Status status
    ) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    public ProjectDTO(@NotNull final UserDTO user, @NotNull final String name) {
        setUserId(user.getId());
        this.name = name;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        result += name;
        if (description != null)
            result += "\t(" + description + ")";
        result += "\t" + Status.toName(status) + "";
        return result;
    }

}
