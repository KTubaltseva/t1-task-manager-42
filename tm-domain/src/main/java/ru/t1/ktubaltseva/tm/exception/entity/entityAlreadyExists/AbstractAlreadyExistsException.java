package ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public abstract class AbstractAlreadyExistsException extends AbstractException {

    public AbstractAlreadyExistsException() {
    }

    public AbstractAlreadyExistsException(@NotNull final String message) {
        super(message);
    }

    public AbstractAlreadyExistsException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public AbstractAlreadyExistsException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
